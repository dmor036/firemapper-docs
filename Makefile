# Makefile for Sphinx documentation
#

# You can set these variables from the command line.
# ehco     =
SPHINXBUILD   = sphinx-build
PAPER         =

# build directories
STANDARD_DIR  = standard_build
ENTERPRISE_DIR  = enterprise_build

# User-friendly check for sphinx-build
ifeq ($(shell which $(SPHINXBUILD) >/dev/null 2>&1; echo $$?), 1)
$(error The '$(SPHINXBUILD)' command was not found. Make sure you have Sphinx installed, then set the SPHINXBUILD environment variable to point to the full path of the '$(SPHINXBUILD)' executable. Alternatively you can add the directory with the executable to your PATH. If you don't have Sphinx installed, grab it from http://sphinx-doc.org/)
endif

# Internal variables.
PAPEROPT_a4     = -D latex_paper_size=a4
PAPEROPT_letter = -D latex_paper_size=letter
ALLSPHINXOPTS   = $(PAPEROPT_$(PAPER)) $(SPHINXOPTS) source

# the i18n builder cannot share the environment and doctrees with the others
I18NSPHINXOPTS  = $(PAPEROPT_$(PAPER)) $(SPHINXOPTS) source

.PHONY: help
help:
	@echo "  html         to make standalone HTML files"
	@echo "  dirhtml      to make HTML files named index.html in directories"
	@echo "  singlehtml   to make a single large HTML file"
	@echo "  latex        to make LaTeX files, you can set PAPER=a4 or PAPER=letter"
	@echo "  latexpdf     to make LaTeX files and run them through pdflatex"
	@echo "  latexpdfja   to make LaTeX files and run them through platex/dvipdfmx"
	@echo "  linkcheck    to check all external links for integrity"


.PHONY: clean
clean:
	rm -rf $(STANDARD_DIR)/*
	rm -rf $(ENTERPRISE_DIR)/*

######################################################################
# FIREMAPPER STANDARD

.PHONY: html
html:
	$(SPHINXBUILD) -b html -t enterprise -d $(ENTERPRISE_DIR)/doctrees $(ALLSPHINXOPTS) $(ENTERPRISE_DIR)/html
	$(SPHINXBUILD) -b html -t standard -d $(STANDARD_DIR)/doctrees $(ALLSPHINXOPTS) $(STANDARD_DIR)/html
	
	@echo "\nBuild finished."
	@echo "-> ./$(ENTERPRISE_DIR)/html/index.html"
	@echo "-> ./$(STANDARD_DIR)/html/index.html\n"

.PHONY: dirhtml
dirhtml:
	$(SPHINXBUILD) -b dirhtml -t enterprise -d $(ENTERPRISE_DIR)/doctrees $(ALLSPHINXOPTS) $(ENTERPRISE_DIR)/dirhtml
	$(SPHINXBUILD) -b dirhtml -t standard -d $(STANDARD_DIR)/doctrees $(ALLSPHINXOPTS) $(STANDARD_DIR)/dirhtml

	@echo "\nBuild finished."
	@echo "-> ./$(ENTERPRISE_DIR)/dirhtml/index.html"
	@echo "-> ./$(STANDARD_DIR)/dirhtml/index.html\n"

.PHONY: singlehtml
singlehtml:
	$(SPHINXBUILD) -b singlehtml -t enterprise -d $(ENTERPRISE_DIR)/doctrees $(ALLSPHINXOPTS) $(ENTERPRISE_DIR)/singlehtml
	$(SPHINXBUILD) -b singlehtml -t standard -d $(STANDARD_DIR)/doctrees $(ALLSPHINXOPTS) $(STANDARD_DIR)/singlehtml

	@echo "\nBuild finished."
	@echo "-> ./$(ENTERPRISE_DIR)/singlehtml/index.html"
	@echo "-> ./$(STANDARD_DIR)/singlehtml/index.html\n"

.PHONY: latex
latex:
	$(SPHINXBUILD) -b latex -t enterprise -d $(ENTERPRISE_DIR)/doctrees $(ALLSPHINXOPTS) $(ENTERPRISE_DIR)/latex
	@echo
	@echo "Build finished; the LaTeX files are in $(ENTERPRISE_DIR)/latex."
	@echo "Run \`make' in that directory to run these through (pdf)latex" \
	      "(use \`make latexpdf' here to do that automatically)."

	$(SPHINXBUILD) -b latex -t standard -d $(STANDARD_DIR)/doctrees $(ALLSPHINXOPTS) $(STANDARD_DIR)/latex
	@echo
	@echo "Build finished; the LaTeX files are in $(STANDARD_DIR)/latex."
	@echo "Run \`make' in that directory to run these through (pdf)latex" \
	      "(use \`make latexpdf' here to do that automatically)."

.PHONY: latexpdf
latexpdf:
	$(SPHINXBUILD) -b latex -t enterprise -d $(ENTERPRISE_DIR)/doctrees $(ALLSPHINXOPTS) $(ENTERPRISE_DIR)/latex
	@echo "Running LaTeX files through pdflatex..."
	$(MAKE) -C $(ENTERPRISE_DIR)/latex all-pdf
	@echo "pdflatex finished; the PDF files are in $(ENTERPRISE_DIR)/latex."

	$(SPHINXBUILD) -b latex -t standard -d $(STANDARD_DIR)/doctrees $(ALLSPHINXOPTS) $(STANDARD_DIR)/latex
	@echo "Running LaTeX files through pdflatex..."
	$(MAKE) -C $(STANDARD_DIR)/latex all-pdf
	@echo "pdflatex finished; the PDF files are in $(STANDARD_DIR)/latex."

.PHONY: latexpdfja
latexpdfja:
	$(SPHINXBUILD) -b latex -t enterprise -d $(ENTERPRISE_DIR)/doctrees $(ALLSPHINXOPTS) $(ENTERPRISE_DIR)/latex
	@echo "Running LaTeX files through platex and dvipdfmx..."
	$(MAKE) -C $(ENTERPRISE_DIR)/latex all-pdf-ja

	$(SPHINXBUILD) -b latex -t standard -d $(STANDARD_DIR)/doctrees $(ALLSPHINXOPTS) $(STANDARD_DIR)/latex
	@echo "Running LaTeX files through platex and dvipdfmx..."
	$(MAKE) -C $(STANDARD_DIR)/latex all-pdf-ja

	@echo "pdflatex finished; the PDF files are in $(STANDARD_DIR)/latex."

.PHONY: linkcheck
linkcheck:
	$(SPHINXBUILD) -b linkcheck -t enterprise -d $(ENTERPRISE_DIR)/doctrees $(ALLSPHINXOPTS) $(ENTERPRISE_DIR)/linkcheck
	$(SPHINXBUILD) -b linkcheck -t standard -d $(STANDARD_DIR)/doctrees $(ALLSPHINXOPTS) $(STANDARD_DIR)/linkcheck

	@echo "Link check complete; look for any errors in the above output or in" \
	      "$(ENTERPRISE_DIR)/linkcheck/output.txt."
		  "$(STANDARD_DIR)/linkcheck/output.txt."


