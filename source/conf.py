import sys, os
sys.path.append(os.path.dirname(__file__))

# print python executable path to help with troubleshooting and required installing packages
print(f"Python: {sys.executable}\n")

# we need to use environment variables to support read the docs.
if tags.has('enterprise'):
    from enterprise import *
    
elif tags.has('standard'):
    from standard import *

elif os.environ.get("tag") == "enterprise":
    # for building enterprise from read the docs
    tags.add('enterprise')
    from enterprise import *

elif os.environ.get("tag") == "standard":
    # for building standard from read the docs
    tags.add('standard')
    from standard import *

else:
    raise Exception(f"You need to build with a tag (e.g. -t enterprise) tags: {tags} env:{os.environ}")

