


.. only :: standard

  FireMapper Standard is designed for individual users for quickly mapping bushfires and other incidents.

  .. note:: The FireMapper Standard manual is been moved to http://docs.firemapper.app/standard/index.html 


.. only :: enterprise

  FireMapper Enterprise is the complete solution for efficiently capturing, distributing and interpreting critical information on the fireground.

  .. note:: The FireMapper Enterprise manual is been moved to http://docs.firemapper.app/enterprise/index.html 

